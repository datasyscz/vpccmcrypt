//
//  VPCCMCrypt_iOS.h
//  VPCCMCrypt iOS
//
//  Created by Petr Zelenka on 12/02/2020.
//  Copyright © 2020 Arx.net. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for VPCCMCrypt_iOS.
FOUNDATION_EXPORT double VPCCMCrypt_iOSVersionNumber;

//! Project version string for VPCCMCrypt_iOS.
FOUNDATION_EXPORT const unsigned char VPCCMCrypt_iOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VPCCMCrypt_iOS/PublicHeader.h>

#import "VPCCM.h"
#import "VPCCMCrypt.h"
