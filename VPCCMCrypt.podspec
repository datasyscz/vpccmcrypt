Pod::Spec.new do |s|
    s.name = "VPCCMCrypt"
    s.version = "0.0.1"
    s.summary = "VPCCMCrypt"

    s.homepage = "http://www.datasys.cz"
    s.license = "MIT"
    s.author = { "Petr Zelenka" => "zelenka@datasys.cz" }
  
    s.platform = :ios, "12.0"

    s.source = { :git => "git@bitbucket.org:datasyscz/vpccmcrypt.git", :branch => "master" }


    s.source_files = [
        "lib",
        "lib/**/*.{h,m}"
    ]

    s.exclude_files = [
        "Classes/Exclude"
    ]

    s.pod_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
    s.user_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }

    s.requires_arc = true
end
